---
layout: default
---
This is the project page for the QGIS plugin and corresponding R package originally developed as part of [Google Summer of Code 2017](https://summerofcode.withgoogle.com/projects/#5197021490184192).

The goal of my project was to create a QGIS Desktop plugin which allows other (‘mapping-deficient’) programming languages (in particular R) to make use of QGIS' data processing and canvas drawing/styling functionalities. The approach of exposing these functions through a plugin also has the particular advantage that one can freely mix of programmatic (command-line scripting) and interactive/QGIS GUI manipulation of geospatial data, as well as simplifying the passing of data between QGIS and R.

[![](screenshot.png "The QGIS plugin and qgisremote R package in action")](screenshot.png)

<!-- code for the screenshot:

library(qgisremote)
iface.newProject()

backgroundlayer <- iface.addTileLayer('http://a.tile.openstreetmap.org/{z}/{x}/{y}.png')

moldova <- raster::getData("GADM", country = "MDA", level = 1)
borderlayer <- iface.addVectorLayer(moldova)

mapCanvas.destinationCrs()
# change to lat/lon for more intuitive zooming on the map canvas
mapCanvas.setDestinationCrs(4326)
mapCanvas.setCenter(28.5, 47.3)
# back to web mercator
mapCanvas.setDestinationCrs(3857)

renderer <- mapLayer.renderer(borderlayer)

# make polygon fill transparent with thick dashed red outline
qgisxml(renderer, 'layer', 'color')
qgisxml(renderer, 'layer', 'color') <- '0,0,0,0'
qgisxml(renderer, 'layer', 'outline_color') <- 'darkred'
qgisxml(renderer, 'layer', 'outline_style') <- 'dash'
qgisxml(renderer, 'layer', 'outline_width') <- 0.5
mapLayer.renderer(borderlayer) <- renderer
-->

The scope of features implemented can be appreciated most immediately by having a look at the functions available from the R package's [function reference](/qgisremote/reference/index.html).

## Links

### `networkapi` QGIS plugin

* [Gitlab repository](https://gitlab.com/qgisapi/networkapi)

### `qgisremote` R package

* [Gitlab repository](https://gitlab.com/qgisapi/qgisremote)
* [HTML Documentation](/qgisremote)
